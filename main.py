from funct import *
from data import *
from pygame.locals import *
from pygame.mixer import *

pygame.init()
pygame.font.init()
pygame.mixer.init()
pygame.key.set_repeat(100,100)
Surface = pygame.display.set_mode((1920,1080),pygame.FULLSCREEN)
pygame.mouse.set_visible(False)
image = GIF_loader()
inProgress = True

map = Map()
perso = map.hero
boss = Mob()
map.group.add(boss)

son_explosion = pygame.mixer.Sound("son/boum.wav")
son_voiture = pygame.mixer.Sound("son/voiture.wav")
son_punchfull = pygame.mixer.Sound("son/punchfull.wav")
son_punchless = pygame.mixer.Sound("son/punchless.wav")
son_die = pygame.mixer.Sound("son/mourir.wav")
son_tir = pygame.mixer.Sound("son/tir.wav")

son_in_game = pygame.mixer.Sound("son/son-in-game.wav")
son_option_intro = pygame.mixer.Sound("son/option_son_intro.wav")
son_option = pygame.mixer.Sound("son/option_son.wav")

npc_1 = npc_1()
npc_2 = npc_2()
npc_3 = npc_3()
npc_4 = npc_4()
npc_5 = npc_5()

car_1 = car_1()
car_2 = car_2()
car_3 = car_3()

buff = Buff()

map.group.add(buff)

map.group.add(npc_1)
map.group.add(npc_2)
map.group.add(npc_3)
map.group.add(npc_4)
map.group.add(npc_5)

quest_giver = Quest_giver()
map.group.add(quest_giver)
quest_giver_sprite_list.add(quest_giver)

quest_hitbox = car_quest_hitbox()
quest_hitbox_sprite_list.add(quest_hitbox)

map.group.add(car_1)
map.group.add(car_2)
map.group.add(car_3)

npc_sprite_list.add(npc_1)
npc_sprite_list.add(npc_2)
npc_sprite_list.add(npc_3)
npc_sprite_list.add(npc_4)
npc_sprite_list.add(npc_5)

buff_sprite_list.add(buff)

car_sprite_list.add(car_1)
car_sprite_list.add(car_2)
car_sprite_list.add(car_3)

voiture = voiture(perso.auto_dammage)
auto_sprite_list.add(voiture)

map.group.add(voiture)

mob_sprite_list.add(boss)

while inProgress :
    if fenetre == 1 or fenetre == 2 or fenetre == 3 :
        if son_on == False and son_intro_done == True:
            son_option.play(loops= -1)
            son_on = True
        if son_on == False:
            son_option_intro.play()
            son_intro_done = True
            son_on = True
            continue
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                inProgress = False
        if fenetre == 1 :
            if event.type == KEYDOWN:
                if event.key == pygame.K_DOWN:
                    choix_menu = 2
                if event.key == pygame.K_UP:
                    choix_menu = 1
                if event.key == pygame.K_RETURN and choix_menu == 1:
                    fenetre = 2
                    break
                if event.key == pygame.K_RETURN and choix_menu == 2:
                    fenetre = 3
                    break
        if fenetre == 2:
            if event.type == KEYDOWN:
                if event.key == pygame.K_LEFT:
                    choix_commande = 1
                if event.key == pygame.K_RIGHT:
                    choix_commande = 2
                if event.key == pygame.K_RETURN and choix_commande == 1:
                    fenetre = 1
                    choix_commande = 2
                    break
                if event.key == pygame.K_RETURN and choix_commande == 2:
                    fenetre = "game"
                    pygame.mixer.stop()
                    son_on = False
                    break
        if fenetre == 3 :
            if event.type == KEYDOWN:
                if event.key == pygame.K_DOWN and choix_option < 3:
                    choix_option += 1
                if event.key == pygame.K_UP and choix_option > 1:
                    choix_option -= 1
                if event.key == pygame.K_RIGHT :
                    if choix_option == 1 and son_sfx != 100:
                        son_sfx += 2
                        break
                    if choix_option == 2 and son_bgm != 100:
                        son_bgm += 2
                        break
                if event.key == pygame.K_LEFT :
                    if choix_option == 1 and son_sfx != 0:
                        son_sfx -= 2
                    if choix_option == 2 and son_bgm != 0:
                        son_bgm -= 2
                if event.key == pygame.K_RETURN and choix_option == 3 :
                    fenetre = 1
                    choix_menu = 1
                    choix_option = 1
                    break
        if fenetre == "game":
            if son_on == False:
                son_in_game.play(loops=-1)
                son_on = True
            Started = True
            if event.type == KEYDOWN:
                if perso.auto == False:
                    if event.key == pygame.K_ESCAPE:
                        inProgress = False
                    if event.key == pygame.K_w:
                        perso.deplacer("up")
                    if event.key == pygame.K_s:
                        perso.deplacer("down")
                    if event.key == pygame.K_a:
                        perso.deplacer("left")
                    if event.key == pygame.K_d:
                        perso.deplacer("right")
                    if event.key == pygame.K_j:
                        perso.shoot(map,son_tir)
                    if event.key == pygame.K_LSHIFT:
                        perso.speed = 2
                    if event.key == pygame.K_k:
                        hitbox = Hitbox(perso)
                        perso.punch()
                        son_punchless.play()
                        if pygame.sprite.spritecollideany(hitbox,mob_sprite_list):
                            pygame.sprite.spritecollideany(hitbox,mob_sprite_list).loosehp()
                            son_punchfull.play()
                            if pygame.sprite.spritecollideany(hitbox,mob_sprite_list).hp == 0 :
                                quest_2 = True
                                pygame.sprite.spritecollideany(hitbox,mob_sprite_list).kill()
                                son_die.play()
                                kill_compteur += 1
                        if pygame.sprite.spritecollideany(hitbox,npc_sprite_list):
                            pygame.sprite.spritecollideany(hitbox,npc_sprite_list).loosehp()
                            if pygame.sprite.spritecollideany(hitbox,npc_sprite_list).hp == 0:
                                pygame.sprite.spritecollideany(hitbox,npc_sprite_list).kill()
                                kill_compteur += 1
                                son_die.play()
                            son_punchfull.play()
                    if event.key == pygame.K_l:
                        hitbox = Hitbox(perso)
                        if pygame.sprite.spritecollideany(hitbox,auto_sprite_list):
                            perso.image = pygame.sprite.spritecollideany(hitbox,auto_sprite_list).image
                            perso.rect = pygame.sprite.spritecollideany(hitbox,auto_sprite_list).image.get_rect()
                            perso.rect.center = pygame.sprite.spritecollideany(hitbox,auto_sprite_list).rect.center
                            pygame.sprite.spritecollideany(hitbox,auto_sprite_list).kill()
                            perso.auto = True
                            perso.speed = 3
                            son_voiture.play()
                            break
                        if pygame.sprite.spritecollideany(hitbox,quest_giver_sprite_list):
                            fenetre = "quest"
                if perso.auto == True:
                    if event.key == pygame.K_ESCAPE:
                        inProgress = False
                    if event.key == pygame.K_w:
                        perso.rouler("up")
                    if event.key == pygame.K_s:
                        perso.rouler("down")
                    if event.key == pygame.K_a:
                        perso.rouler("left")
                    if event.key == pygame.K_d:
                        perso.rouler("right")
                    if event.key == pygame.K_l:
                        perso.auto = False
                        voiture.__init__(perso.auto_dammage)
                        voiture.rect.topleft = perso._position
                        voiture.auto_dammage = perso.auto_dammage
                        auto_sprite_list.add(voiture)
                        map.group.add(voiture)
                        perso.image = pygame.image.load("ressource/Hero.gif").convert_alpha()
                        perso.image = pygame.transform.scale(perso.image,(20,30))
                        perso.rect = perso.image.get_rect()
                        perso.rect.center = (perso._position[0]+20,perso._position[1]+80)
                        perso.speed = 1
                        son_voiture.play()
                        break
                if Pause == False and event.key == K_RETURN:
                    Pause = True
                    fenetre = "pause"
                    perso.stop()
                    break
                if Check_Progress == False and event.key == K_TAB:
                    Check_Progress = True
                    fenetre = "Check Progress"
                    perso.stop()
                    break
                if event.key == pygame.K_SEMICOLON:
                        map.map_layer.zoom = 1
            if event.type == KEYUP:
                if event.type == KEYUP:
                    if event.key in (pygame.K_w,pygame.K_s,pygame.K_a,pygame.K_d):
                        perso.stop()
                if perso.auto == False :
                    if event.key == pygame.K_k:
                        perso.deplacer(perso.movement)
                        perso.stop()
                    if event.key == pygame.K_LSHIFT:
                        perso.speed = 1
                if event.key == pygame.K_SEMICOLON:
                    map.map_layer.zoom = 4
            perso.update()
        if fenetre == "pause":
            if event.type == KEYDOWN:
                if event.key == pygame.K_DOWN and choix_option < 3:
                    choix_option += 1
                if event.key == pygame.K_UP and choix_option > 1:
                    choix_option -= 1
                if event.key == pygame.K_RIGHT :
                    if choix_option == 1 and son_sfx != 100:
                        son_sfx += 2
                        break
                    if choix_option == 2 and son_bgm != 100:
                        son_bgm += 2
                        break
                if event.key == pygame.K_LEFT :
                    if choix_option == 1 and son_sfx != 0:
                        son_sfx -= 2
                    if choix_option == 2 and son_bgm != 0:
                        son_bgm -= 2
                if event.key == pygame.K_RETURN and choix_option == 3 :
                    fenetre = "game"
                    choix_menu = 1
                    choix_option = 1
                    Pause = False
                    break
        if fenetre == "Check Progress":
            if event.type == KEYUP:
                if event.key == K_TAB:
                    Check_Progress = False
                    fenetre = "game"
        if fenetre == "quest":
            if event.type == KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    inProgress = False
                if event.key == pygame.K_l and quest_dialogue == 0:
                    quest_dialogue = 1
                    continue
                if event.key == pygame.K_l and quest_dialogue == 1:
                    quest_dialogue = 2
                    continue
                if event.key == pygame.K_l and quest_dialogue == 2:
                    quest_dialogue = 3
                    continue
                if event.key == pygame.K_l and quest_dialogue == 3:
                    fenetre = "game"
                    quest_dialogue = 0
                    continue
            fenetre_quest(quest_dialogue)
            pygame.display.update()
    if fenetre == "game":
        if safe_time == 100:
            if pygame.sprite.spritecollideany(perso,mob_sprite_list):
                perso.hp -= 10
                safe_time = 0
                lunch_safe_time = True
            if pygame.sprite.spritecollideany(perso,car_sprite_list) and perso.auto == False:
                perso.hp -= 50
                safe_time = 0
                lunch_safe_time = True
            if perso.hp == 0:
                perso.kill()
                son_die.play()
                Wasted = True
                perso.health = 0
            if pygame.sprite.spritecollideany(perso,car_sprite_list):
                if perso.auto == True :
                    perso.auto_dammage += 1
                    safe_time = 0
                    lunch_safe_time = True
            if perso.auto_dammage == 10 :
                perso.kill()
                son_explosion.play()
                son_die.play()
                Wasted = True
                perso.health = 0
        if lunch_safe_time == True :
            safe_time += 1
            if safe_time > 100:
                safe_time = 100
                lunch_safe_time = False
        if pygame.sprite.spritecollideany(perso,car_sprite_list):
            if perso.dir == "H":
                perso.rect.x -= perso.speed_x*2
            if perso.dir == "V" :
                perso.rect.y -= perso.speed_y*2
        auto_health = font_korean_50.render ("Auto-health : "+str(10-perso.auto_dammage),True,Red)
        health = font_korean_50.render("Health : "+str(perso.hp),True,Red)
        map.update()
        map.draw(Surface)
        if perso.auto == True :
            Surface.blit(auto_health,(50,75))
        Surface.blit(health,(20,20))
        pygame.display.update()
        if pygame.sprite.groupcollide(mob_sprite_list,bullet_sprite_list,True,True):
            son_die.play()
            quest_2 = True
            kill_compteur += 1
        if pygame.sprite.groupcollide(npc_sprite_list,bullet_sprite_list,True,True):
            son_die.play()
            kill_compteur += 1
        if pygame.sprite.spritecollideany(perso,auto_sprite_list):
            if perso.dir == "H":
                perso.rect.x -= perso.speed_x*2
            if perso.dir == "V" :
                perso.rect.y -= perso.speed_y*2
        if pygame.sprite.spritecollideany(perso,buff_sprite_list):
            perso.armed = True
            buff.kill()
            quest_1 = True
        if pygame.sprite.spritecollideany(perso,npc_sprite_list) and perso.auto == False:
            if perso.dir == "H":
                perso.rect.x -= perso.speed_x*2
            if perso.dir == "V" :
                perso.rect.y -= perso.speed_y*2
        if pygame.sprite.spritecollideany(perso,quest_giver_sprite_list):
            if perso.dir == "H":
                perso.rect.x -= perso.speed_x*2
            if perso.dir == "V" :
                perso.rect.y -= perso.speed_y*2
        if pygame.sprite.spritecollideany(perso,npc_sprite_list) and perso.auto == True:
            pygame.sprite.spritecollideany(perso,npc_sprite_list).kill()
            son_die.play()
            kill_compteur += 1
        if perso.auto == True :
            if pygame.sprite.spritecollideany(perso,quest_hitbox_sprite_list):
                quest_3 = True
        respawn += 1
        if respawn == 1000:
            if npc_1 not in npc_sprite_list:
                npc_1.__init__()
                npc_sprite_list.add(npc_1)
                map.group.add(npc_1)
            if npc_2 not in npc_sprite_list:
                npc_2.__init__()
                npc_sprite_list.add(npc_2)
                map.group.add(npc_2)
            if npc_3 not in npc_sprite_list:
                npc_3.__init__()
                npc_sprite_list.add(npc_3)
                map.group.add(npc_3)
            if npc_4 not in npc_sprite_list:
                npc_4.__init__()
                npc_sprite_list.add(npc_4)
                map.group.add(npc_4)
            if npc_5 not in npc_sprite_list:
                npc_5.__init__()
                npc_sprite_list.add(npc_5)
                map.group.add(npc_5)
            respawn = 0
    if fenetre == 1 or fenetre == 2 or fenetre == 3 :
        image.load(fenetre)
        image.update(choix_menu,choix_option,choix_commande,son_bgm,son_sfx)
        pygame.display.update()
    if fenetre == "pause":
        fenetre = 3
        image.load(fenetre)
        image.update(choix_menu,choix_option,choix_commande,son_bgm,son_sfx)
        pygame.display.update()
        fenetre = "pause"
    if fenetre == "Check Progress":
        fenetre_progress(quest_1,quest_2,quest_3,kill_compteur,perso.compteur)
        pygame.display.update()
    regenhp += 1
    if regenhp == 1000:
        if perso.hp == 100 :
            pass
        else:
            perso.hp += 10
        regenhp = 0
    if Wasted == True :
        fenetre = "wasted"
        Surface.blit(Wasted_img,(700,500))
        pygame.display.update()
    son_explosion.set_volume(son_sfx/100)
    son_voiture.set_volume(son_sfx/100)
    son_punchless.set_volume(son_sfx/100)
    son_punchfull.set_volume(son_sfx/100)
    son_die.set_volume(son_sfx/100)
    son_tir.set_volume(son_sfx/100)
    son_in_game.set_volume(son_bgm/100)
    son_option.set_volume(son_bgm/100)
    son_option_intro.set_volume(son_bgm/100)
    if pygame.mixer.get_busy()== False:
        son_on = False
pygame.quit()