import pygame
pygame.init()
pygame.font.init()

White = (255,255,255)
Silver =(220,220,220)
Gray = (128,128,128)
Black = (0,0,0)
Red = (255,0,0)
Maroon = (128,0,0)
Yellow = (255,255,0)
Olive = (128,128,0)
Lime = (0,255,0)
Green = (0,128,0)
Aqua = (0,255,255)
Teal = (0,128,128)
Blue = (0,0,255)
Navy = (0,0,128)
Fuchsia = (255,0,255)
Purple = (128,0,128)
Orange = (255,165,0)
Pink = (255,227,235)

fenetre = 1
choix_menu = 1
choix_option = 1
choix_commande = 2
son_sfx = 50
son_bgm = 50
regenhp = 0
respawn = 0
Pause = False
Check_Progress = False
Started = False
Wasted = False
lunch_safe_time = False
safe_time = 100
quest_dialogue = 0

font_korean_80 = pygame.font.Font("font/korean.ttf",80)
font_korean_50 = pygame.font.Font("font/korean.ttf",50)
font_railway_90 = pygame.font.Font("font/railway_to_hells.ttf",90)
font_railway_100 = pygame.font.Font("font/railway_to_hells.ttf",100)
font_warrior_70 = pygame.font.Font("font/warrior.ttf",70)
font_warrior_80 = pygame.font.Font("font/warrior.ttf",80)
font_japkey_90 = pygame.font.Font("font/jap_keyboard.ttf",90)
font_japkey_190 = pygame.font.Font("font/jap_keyboard.ttf",190)
font_gyosho_90 = pygame.font.Font("font/gyosho.ttf",90)
font_gyosho_70 = pygame.font.Font("font/gyosho.ttf",70)

font_warrior_80.set_bold(True)

commande_z = font_japkey_90.render("z",True,Orange)
commande_q = font_japkey_90.render("q",True,Orange)
commande_s = font_japkey_90.render("s",True,Orange)
commande_d = font_japkey_90.render("d",True,Orange)
commande_j = font_japkey_90.render("j",True,Orange)
commande_k = font_japkey_90.render("k",True,Orange)
commande_l = font_japkey_90.render("l",True,Orange)
commande_return = font_japkey_190.render(chr(209),True,Orange)
commande_shift = font_japkey_90.render(chr(182),True,Orange)
commande_tab = font_japkey_90.render(chr(198),True,Orange)
commande_m = font_japkey_90.render("m",True,Orange)

description_j = font_korean_80.render("punch",True,Orange)
description_k = font_korean_80.render("shoot",True,Orange)
description_l = font_korean_80.render("interact",True,Orange)
description_m = font_korean_80.render("maps",True,Orange)
description_tab = font_korean_80.render("check progress",True,Orange)
description_shift = font_korean_80.render("sprint",True,Orange)
description_return = font_korean_80.render("pause",True,Orange)
description_zqsd = font_korean_80.render("move",True,Orange)

title = font_railway_100.render("Vigilante",True,Orange)
aile_gauche = font_railway_90.render("[",True, Orange)
aile_droite = font_railway_90.render("]",True, Orange)
start = font_warrior_70.render("Start",True,Maroon)
option = font_warrior_70.render("Option",True,Maroon)
start_2 = font_warrior_80.render("Start",True,Orange)
option_2 = font_warrior_80.render("Option",True,Orange)

option_title = font_gyosho_90.render("Option",True, Maroon)

Wasted_img = font_railway_100.render("WASTED",True, Red)

mob_sprite_list = pygame.sprite.Group()
npc_sprite_list = pygame.sprite.Group()
bullet_sprite_list = pygame.sprite.Group()
buff_sprite_list = pygame.sprite.Group()
car_sprite_list = pygame.sprite.Group()
auto_sprite_list = pygame.sprite.Group()
quest_hitbox_sprite_list = pygame.sprite.Group()
quest_giver_sprite_list = pygame.sprite.Group()

quest_1 = False
quest_2 = False
quest_3 = False

son_on = False
son_intro_done = False

kill_compteur = 0


