import pyscroll
import pyscroll.data
from pytmx.util_pygame import load_pygame
from pyscroll.group import PyscrollGroup
from data import *
from random import randint

test = 0

width = pygame.display.Info().current_w
height = pygame.display.Info().current_h
pygame.init()
pygame.font.init()
Surface = pygame.display.set_mode((2560, 1600))


def fenetre_base(choix):
    Surface.blit(title, (1000, 290))
    Surface.blit(aile_gauche, (890, 290))
    Surface.blit(aile_droite, (1620, 290))
    if choix == 1:
        Surface.blit(start_2, (1190, 550))
        Surface.blit(option, (1200, 700))
    if choix == 2:
        Surface.blit(start, (1210, 550))
        Surface.blit(option_2, (1170, 700))


def fenetre_1(choix):
    Surface.blit(commande_z, (1200, 125))
    Surface.blit(commande_q, (1200, 210))
    Surface.blit(commande_s, (1110, 210))
    Surface.blit(commande_d, (1290, 210))
    Surface.blit(commande_j, (200, 100))
    Surface.blit(commande_k, (200, 240))
    Surface.blit(commande_l, (200, 380))
    Surface.blit(commande_m, (200, 520))
    Surface.blit(commande_tab, (200, 660))
    Surface.blit(commande_shift, (1170, 550))
    Surface.blit(commande_return, (1170, 330))
    Surface.blit(description_j, (350, 100))
    Surface.blit(description_k, (350, 240))
    Surface.blit(description_l, (350, 380))
    Surface.blit(description_m, (350, 520))
    Surface.blit(description_tab, (350, 660))
    Surface.blit(description_zqsd, (1450, 170))
    Surface.blit(description_return, (1450, 380))
    Surface.blit(description_shift, (1450, 550))

    if choix == 1:
        choix_return = font_gyosho_90.render("Return", True, Orange)
        choix_continue = font_gyosho_90.render("Continuer", True, Maroon)
        Surface.blit(choix_return, (20, 980))
        Surface.blit(choix_continue, (1450, 980))
    if choix == 2:
        choix_return = font_gyosho_90.render("Return", True, Maroon)
        choix_continue = font_gyosho_90.render("Continuer", True, Orange)
        Surface.blit(choix_return, (20, 980))
        Surface.blit(choix_continue, (1450, 980))


def fenetre_2(son_bgm, son_sfx, choix):
    Surface.blit(option_title, (800, 50))

    pygame.draw.rect(Surface, Aqua, (600, 300, 800, 50), 0)
    pygame.draw.rect(Surface, Blue, (600, 300, son_sfx * 8, 50), 0)
    pygame.draw.rect(Surface, Aqua, (600, 500, 800, 50), 0)
    pygame.draw.rect(Surface, Blue, (600, 500, son_bgm * 8, 50), 0)
    if choix == 1:
        option_sfx = font_korean_50.render("Effect sound", True, Orange)
        option_bgm = font_korean_50.render("Background Music", True, Maroon)
        son_sfx_txt = font_korean_50.render(str(son_sfx), True, Orange)
        son_bgm_txt = font_korean_50.render(str(son_bgm), True, Maroon)
        option_return = font_gyosho_90.render("Return", True, Maroon)
        Surface.blit(son_sfx_txt, (1430, 300))
        Surface.blit(option_sfx, (240, 300))
        Surface.blit(son_bgm_txt, (1430, 500))
        Surface.blit(option_bgm, (200, 500))
        Surface.blit(option_return, (30, 970))
    if choix == 2:
        option_sfx = font_korean_50.render("Effect sound", True, Maroon)
        option_bgm = font_korean_50.render("Background Music", True, Orange)
        son_sfx_txt = font_korean_50.render(str(son_sfx), True, Maroon)
        son_bgm_txt = font_korean_50.render(str(son_bgm), True, Orange)
        option_return = font_gyosho_90.render("Return", True, Maroon)
        Surface.blit(son_bgm_txt, (1430, 500))
        Surface.blit(option_bgm, (200, 500))
        Surface.blit(son_sfx_txt, (1430, 300))
        Surface.blit(option_sfx, (240, 300))
        Surface.blit(option_return, (30, 970))
    if choix == 3:
        option_sfx = font_korean_50.render("Effect sound", True, Maroon)
        option_bgm = font_korean_50.render("Background Music", True, Maroon)
        son_sfx_txt = font_korean_50.render(str(son_sfx), True, Maroon)
        son_bgm_txt = font_korean_50.render(str(son_bgm), True, Maroon)
        option_return = font_gyosho_90.render("Return", True, Orange)
        Surface.blit(son_bgm_txt, (1430, 500))
        Surface.blit(option_bgm, (200, 500))
        Surface.blit(son_sfx_txt, (1430, 300))
        Surface.blit(option_sfx, (240, 300))
        Surface.blit(option_return, (30, 970))


def fenetre_progress(quest_1, quest_2, quest_3, kill_compteur, perso_compteur):
    pygame.draw.rect(Surface, (0, 102, 204), (300, 150, 1300, 760), 0)
    pygame.draw.rect(Surface, (153, 0, 0), (300, 200, 1300, 100), 0)
    pygame.draw.rect(Surface, White, (300, 200, 1300, 100), 4)
    pygame.draw.rect(Surface, White, (300, 150, 1300, 760), 4)
    kfont = pygame.font.Font("font/korean.ttf", 60)
    kfont2 = pygame.font.Font("font/korean.ttf", 80)
    Surface.blit(kfont2.render("Check progress", 1, (0, 0, 0)), (720, 210))
    Surface.blit(kfont.render("Quest: ", 1, (0, 0, 0)), (325, 325))
    Surface.blit(kfont.render("     Pick up object", 1, (0, 0, 0)), (325, 400))
    Surface.blit(kfont.render("     Kill the NPC", 1, (0, 0, 0)), (325, 475))
    Surface.blit(kfont.render("     Take car to the station", 1, (0, 0, 0)), (325, 550))
    Surface.blit(kfont.render("Kills: ", 1, (0, 0, 0)), (325, 675))
    Surface.blit(kfont.render("Meters walked: ", 1, (0, 0, 0)), (325, 800))
    if quest_1 == True:
        Surface.blit(kfont.render("Done", True, Green), (1300, 400))
    else:
        Surface.blit(kfont.render("Not done", True, Red), (1250, 400))
    if quest_2 == True:
        Surface.blit(kfont.render("Done", True, Green), (1300, 475))
    else:
        Surface.blit(kfont.render("Not done", True, Red), (1250, 475))
    if quest_3 == True:
        Surface.blit(kfont.render("Done", True, Green), (1300, 550))
    else:
        Surface.blit(kfont.render("Not done", True, Red), (1250, 550))
    Surface.blit(kfont.render(str(kill_compteur), True, Orange), (1300, 675))
    Surface.blit(kfont.render(str(perso_compteur / 10) + "meter", True, Orange), (1300, 800))
    pygame.display.update()


def fenetre_quest(dialogue):
    Surface.blit(pygame.transform.scale(pygame.image.load("img/Npcq.gif"), (450, 500)), (1480, 300))
    pygame.draw.rect(Surface, (0, 102, 204), (5, 800, 1909, 275), 0)
    pygame.draw.rect(Surface, Silver, (5, 800, 1909, 275), 10)
    kfont = pygame.font.Font("font/gyosho.ttf", 80)
    kfont2 = pygame.font.Font("font/shrimp.ttf", 100)
    Surface.blit(kfont.render("Ashley", 1, Red), (50, 680))
    if dialogue == 1:
        Surface.blit(kfont2.render("Hello sir ... I have some request ... Can you help me ? ", 1, (0, 0, 0)), (50, 840))
        Surface.blit(kfont2.render("My brother have been kidnap by the bad corp", 1, (0, 0, 0)), (50, 950))
    if dialogue == 2:
        Surface.blit(kfont2.render("I need you to pick up the hidden weapon in the village", 1, (0, 0, 0)), (50, 840))
        Surface.blit(kfont2.render("and beat the corp's boss, near the tower", 1, (0, 0, 0)), (50, 950))
    if dialogue == 3:
        Surface.blit(kfont2.render("And then take us in this car to the station in the city", 1, (0, 0, 0)), (50, 840))
        Surface.blit(kfont2.render("You will be my hero ... ", 1, (0, 0, 0)), (50, 950))
    pygame.display.update()


class GIF_loader:
    def __init__(self):
        self.num = 1

    def load(self, GIF):
        self.GIF = GIF
        try:
            try:
                self.image = pygame.image.load("img/GIF_" + str(GIF) + "/frame-0" + str(self.num) + ".gif")
            except:
                self.image = pygame.image.load("img/GIF_" + str(GIF) + "/frame-" + str(self.num) + ".gif")
        except:
            self.num = 1
            self.image = pygame.image.load("img/GIF_" + str(GIF) + "/frame-0" + str(self.num) + ".gif")
        self.image = pygame.transform.scale(self.image,
                                            (pygame.display.Info().current_w, pygame.display.Info().current_h))

    def update(self, choix_menu, choix_option, choix_commande, son_bgm, son_sfx):
        for i in range(10):
            Surface.blit(self.image, (0, 0))
            if self.GIF == 1:
                fenetre_base(choix_menu)
            if self.GIF == 2:
                fenetre_1(choix_commande)
            if self.GIF == 3:
                fenetre_2(son_bgm, son_sfx, choix_option)
        self.num += 1


class Player(pygame.sprite.Sprite):
    speed = 1

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(pygame.image.load("ressource/Hero12.gif").convert_alpha(), (20, 30))
        self.rect = self.image.get_rect()
        self._position = [1140, 2320]
        self._back = self.position
        self.rect.center = self._position
        self.speed_x = 0
        self.speed_y = 0
        self.movement = "up"
        self.dir = ""
        self.hp = 100
        self.feet = pygame.Rect(0, 0, self.rect.width * .5, 8)
        self.armed = False
        self.auto = False
        self.auto_dammage = 0
        self.compteur = 0

    @property
    def position(self):
        return list(self._position)

    @position.setter
    def position(self, value):
        self._position = list(value)

    def shoot(self, map, son_tir):
        if self.armed == True:
            bullet = Bullet(self.rect.center, self.movement)
            map.group.add(bullet)
            bullet_sprite_list.add(bullet)
            son_tir.play()

    def update(self):
        self._back = self._position
        if self.dir == "H":
            self.rect.x += self.speed_x
            self.compteur += abs(self.speed_y)
        if self.dir == "V":
            self.rect.y += self.speed_y
            self.compteur += abs(self.speed_x)
        self._position = [self.rect.x, self.rect.y]
        self.rect.topleft = self._position
        self.feet.midbottom = self.rect.midbottom

    def deplacer(self, movement):
        if movement == "up":
            self.speed_y = -self.speed
            self.dir = "V"
            self.image = pygame.image.load("ressource/Hero12.gif")
        if movement == "down":
            self.speed_y = self.speed
            self.dir = "V"
            self.image = pygame.image.load("ressource/Hero.gif")
        if movement == "left":
            self.speed_x = -self.speed
            self.dir = "H"
            self.image = pygame.image.load("ressource/Hero5.gif")
        if movement == "right":
            self.speed_x = self.speed
            self.dir = "H"
            self.image = pygame.image.load("ressource/Hero10.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.movement = movement

    def rouler(self, movement):
        if movement == "up":
            self.speed_y = -self.speed
            self.dir = "V"
            self.image = pygame.image.load("cars/voiture2.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
        if movement == "down":
            self.speed_y = self.speed
            self.dir = "V"
            self.image = pygame.image.load("cars/voiture4.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
        if movement == "left":
            self.speed_x = -self.speed
            self.dir = "H"
            self.image = pygame.image.load("cars/voiture.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
        if movement == "right":
            self.speed_x = self.speed
            self.dir = "H"
            self.image = pygame.image.load("cars/voiture3.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
        self.movement = movement

    def stop(self):
        self.speed_x = 0
        self.speed_y = 0

    def punch(self):
        if self.movement == "up":
            self.image = pygame.image.load("ressource/Hero7.gif")
        if self.movement == "down":
            self.image = pygame.image.load("ressource/Hero2.gif")
        if self.movement == "left":
            self.image = pygame.image.load("ressource/Hero3.gif")
        if self.movement == "right":
            self.image = pygame.image.load("ressource/Hero8.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))


class Bullet(pygame.sprite.Sprite):
    speed = -10

    def __init__(self, position, movement):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("guns/bullets.gif").convert_alpha()
        self.image = pygame.transform.scale(self.image, (1, 3))
        self.rect = self.image.get_rect()
        self.rect.center = position
        self.movement = movement
        self.feet = pygame.Rect(0, 0, self.rect.width * .5, 8)

    def update(self):
        if self.movement == "up":
            self.image = pygame.image.load("guns/bullets4.gif")
            self.image = pygame.transform.scale(self.image, (3, 9))
            self.rect.y += self.speed
        if self.movement == "down":
            self.image = pygame.image.load("guns/bullets2.gif")
            self.image = pygame.transform.scale(self.image, (3, 9))
            self.rect.y -= self.speed
        if self.movement == "left":
            self.image = pygame.image.load("guns/bullets3.gif")
            self.image = pygame.transform.scale(self.image, (9, 3))
            self.rect.x += self.speed
        if self.movement == "right":
            self.image = pygame.image.load("guns/bullets.gif")
            self.image = pygame.transform.scale(self.image, (9, 3))
            self.rect.x -= self.speed
        if self.rect.x < 0 or self.rect.x > 2400:
            self.kill()
        if self.rect.y < 0 or self.rect.y > 2400:
            self.kill()


class Mob(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/Mechant.gif").convert_alpha()
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect = self.image.get_rect()
        self._position = [1118, 1699]
        self.rect.center = self._position
        self.hp = 20
        self.feet = pygame.Rect(0, 0, self.rect.width * .5, 8)
        self.dir = ""

    def loosehp(self):
        self.hp -= 1

    def update(self):
        pass


class npc_1(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/fille4.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect = self.image.get_rect()
        self.rect.topleft = [2271, 342]
        self.speed = 1
        self.hp = 4

    def update(self):
        if self.rect.topleft == (2271, 342):
            self.speed = 1
            self.image = pygame.image.load("ressource/fille4.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
        if self.rect.topleft == (2271, 1348):
            self.speed = -1
            self.image = pygame.image.load("ressource/fille7.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect.y += self.speed

    def loosehp(self):
        self.hp -= 1


class npc_2(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/papi5.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect = self.image.get_rect()
        self.rect.topleft = [1392, 350]
        self.speed = 1
        self.dir = "V"
        self.hp = 4

    def update(self):
        if self.rect.topleft == (1392, 350) and self.dir == "V":
            self.speed = 1
            self.image = pygame.image.load("ressource/papi2.gif")
            self.image = pygame.transform.flip(self.image, True, False)
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (1392, 350) and self.dir == "H":
            self.speed = 1
            self.image = pygame.image.load("ressource/papi5.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        elif self.rect.topleft == (1392, 1231):
            self.speed = -1
            self.image = pygame.image.load("ressource/papi7.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        elif self.rect.topleft == (2266, 350):
            self.speed = -1
            self.image = pygame.image.load("ressource/papi2.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed

    def loosehp(self):
        self.hp -= 1


class npc_3(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/persof4.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect = self.image.get_rect()
        self.rect.topleft = [1614, 1060]
        self.speed = 1
        self.dir = "V"
        self.hp = 4

    def update(self):
        if self.rect.topleft == (1614, 1060):
            self.speed = 1
            self.image = pygame.image.load("ressource/persof10.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (2031, 1060):
            self.speed = -1
            self.image = pygame.image.load("ressource/persof.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        elif self.rect.topleft == (2031, 897):
            self.speed = -1
            self.image = pygame.image.load("ressource/persof7.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (1614, 897):
            self.speed = 1
            self.image = pygame.image.load("ressource/persof4.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed

    def loosehp(self):
        self.hp -= 1


class npc_4(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/persom5.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect = self.image.get_rect()
        self.rect.topleft = [478, 368]
        self.speed = 1
        self.dir = "V"
        self.hp = 4

    def update(self):
        if self.rect.topleft == (478, 368):
            self.speed = 1
            self.image = pygame.image.load("ressource/persom5.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        elif self.rect.topleft == (478, 863) and self.dir == "V":
            self.speed = 1
            self.image = pygame.image.load("ressource/persom2.gif")
            self.image = pygame.transform.flip(self.image, True, False)
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (876, 863):
            self.speed = -1
            self.image = pygame.image.load("ressource/persom2.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (478, 863) and self.dir == "H":
            self.speed = -1
            self.image = pygame.image.load("ressource/persom7.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed

    def loosehp(self):
        self.hp -= 1


class npc_5(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/persofem5.gif")
        self.image = pygame.transform.scale(self.image, (20, 30))
        self.rect = self.image.get_rect()
        self.rect.topleft = [476, 1349]
        self.speed = 1
        self.dir = "V"
        self.hp = 4

    def update(self):
        if self.rect.topleft == (476, 1349):
            self.speed = 1
            self.image = pygame.image.load("ressource/persofem2.gif")
            self.image = pygame.transform.flip(self.image, True, False)
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (874, 1349):
            self.speed = -1
            self.image = pygame.image.load("ressource/persofem7.gif")
            self.image = pygame.transform.flip(self.image, True, False)
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        elif self.rect.topleft == (874, 1042):
            self.speed = -1
            self.image = pygame.image.load("ressource/persofem2.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "H"
        elif self.rect.topleft == (476, 1042):
            self.speed = 1
            self.image = pygame.image.load("ressource/persofem5.gif")
            self.image = pygame.transform.scale(self.image, (20, 30))
            self.dir = "V"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed

    def loosehp(self):
        self.hp -= 1


class car_1(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("cars/camiond.gif")
        self.image = pygame.transform.scale(self.image, (70, 60))
        self.rect = self.image.get_rect()
        self.rect.center = [2230, 420]
        self.speed = 2
        self.dir = "V"

    def update(self):
        if self.rect.center == (2230, 420):
            self.speed = -2
            self.image = pygame.image.load("cars/camiond.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
            self.dir = "H"
        elif self.rect.center == (1482, 420):
            self.speed = 2
            self.image = pygame.image.load("cars/camiond2.gif")
            self.image = pygame.transform.flip(self.image, True, False)
            self.image = pygame.transform.scale(self.image, (40, 80))
            self.dir = "V"
        elif self.rect.center == (1482, 1198):
            self.speed = 2
            self.image = pygame.image.load("cars/camiond3.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
            self.dir = "H"
        elif self.rect.center == (2230, 1198):
            self.speed = -2
            self.image = pygame.image.load("cars/camiond4.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
            self.dir = "V"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed


class car_2(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("cars/truck3.gif")
        self.image = pygame.transform.scale(self.image, (70, 60))
        self.rect = self.image.get_rect()
        self.rect.center = [1554, 486]
        self.speed = 2
        self.dir = "V"

    def update(self):
        if self.rect.center == (1554, 486):
            self.speed = 2
            self.image = pygame.image.load("cars/truck2.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
            self.dir = "H"
        elif self.rect.center == (2154, 486):
            self.speed = 2
            self.image = pygame.image.load("cars/truck3.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
            self.dir = "V"
        elif self.rect.center == (2154, 736):
            self.speed = -2
            self.image = pygame.image.load("cars/truck.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
            self.dir = "H"
        elif self.rect.center == (1554, 736):
            self.speed = -2
            self.image = pygame.image.load("cars/truck4.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
            self.dir = "V"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed


class car_3(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("cars/voiture3.gif")
        self.image = pygame.transform.scale(self.image, (70, 60))
        self.rect = self.image.get_rect()
        self.rect.center = [2150, 808]
        self.speed = 2
        self.dir = "V"

    def update(self):
        if self.rect.center == (1556, 808):
            self.speed = 2
            self.image = pygame.image.load("cars/voiture3.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
            self.dir = "H"
        elif self.rect.center == (2150, 808):
            self.speed = 2
            self.image = pygame.image.load("cars/voiture4.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
            self.dir = "V"
        elif self.rect.center == (2150, 1132):
            self.speed = -2
            self.image = pygame.image.load("cars/voiture.gif")
            self.image = pygame.transform.scale(self.image, (70, 60))
            self.dir = "H"
        elif self.rect.center == (1556, 1132):
            self.speed = -2
            self.image = pygame.image.load("cars/voiture2.gif")
            self.image = pygame.transform.scale(self.image, (40, 80))
            self.dir = "V"
        if self.dir == "H":
            self.rect.x += self.speed
        elif self.dir == "V":
            self.rect.y += self.speed


class voiture(pygame.sprite.Sprite):
    def __init__(self, auto_dammage):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("cars/voiture3.gif")
        self.image = pygame.transform.scale(self.image, (70, 60))
        self.rect = self.image.get_rect()
        self.rect.center = [1032, 2200]
        self.auto_dammage = auto_dammage

    def update(self):
        pass


class Hitbox(pygame.sprite.Sprite):
    def __init__(self, perso):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([100, 100])
        self.rect = self.image.get_rect()
        self.rect.center = perso.rect.center

    def update(self):
        pass


class Buff(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("guns/gun.gif")
        self.image = pygame.transform.scale(self.image, (20, 10))
        self.rect = self.image.get_rect()
        self._position = [676, 630]
        self.rect.center = self._position

    def update(self):
        pass


class Quest_giver(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ressource/persoquest.gif")
        self.image = pygame.transform.scale(self.image, (25, 30))
        self.rect = self.image.get_rect()
        self.rect.topleft = [1126, 2252]

    def update(self):
        pass


class car_quest_hitbox(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([100, 100])
        self.rect = self.image.get_rect()
        self.rect.center = [1830, 350]


class Map(pygame.sprite.Sprite):
    def __init__(self):
        tmx_data = load_pygame("tile/ville_gare.tmx")
        map_data = pyscroll.TiledMapData(tmx_data)
        self.map_layer = pyscroll.BufferedRenderer(map_data, Surface.get_size())
        self.group = PyscrollGroup(map_layer=self.map_layer, default_layer=16)
        self.map_layer.zoom = 4
        self.hero = Player()
        self.hero.position = self.map_layer.map_rect.center
        self.group.add(self.hero)
        self.objet = list()
        for object in tmx_data.objects:
            self.objet.append(pygame.Rect(object.x, object.y, object.width, object.height))

    def draw(self, Surface):
        self.group.center(self.hero.rect.center)
        self.group.draw(Surface)

    def update(self):
        self.group.update()
        if self.hero.feet.collidelist(self.objet) > -1:
            if self.hero.dir == "H":
                self.hero.rect.x -= self.hero.speed_x * 2
            if self.hero.dir == "V":
                self.hero.rect.y -= self.hero.speed_y * 2
